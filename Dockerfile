FROM debian:stable-slim

RUN apt update && \
	apt install -y apt-transport-https lsb-release ca-certificates wget 
RUN wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg

RUN echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/php.list 

RUN apt update && \
	apt install -y libapache2-mod-php7.2 apache2 php7.2-mysql php7.2-opcache php7.2-readline php7.2-mbstring php7.2-ldap php7.2-json php7.2-gd php7.2-curl php-apcu-bc php-apcu

CMD apachectl -D FOREGROUND
